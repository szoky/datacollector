/****************************************************************************
*                                                                           *
*  This program queries the PREDICT server and collects the current data    *
*  from the MO-110 (SMOG-1) and DO-111 (DIY 1) satellites and saves it to   *
*  the local directory in CSV format.                                        *
*  The -i option can be used to specify the polling interval. The default   *
*  interval is 10 minutes.                                                  *
*                                                                           *
*             Author of the data collection part: HA71970                   *
*                                                                           *
*****************************************************************************
*                                                                           *
*                    Last modified on 12-October-2021                       *
*                                                                           *
*****************************************************************************
*                                                                           *
* This program is free software; you can redistribute it and/or modify it   *
* under the terms of the GNU General Public License as published by the     *
* Free Software Foundation; either version 2 of the License or any later    *
* version.                                                                  *
*                                                                           *
* This program is distributed in the hope that it will useful, but WITHOUT  *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     *
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details.                                                         *
*                                                                           *
*****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <signal.h>
#include <errno.h>
#include <arpa/inet.h>

#define FILENAME "./satdatalog.csv"
#define SAT_1 "MO-110"
#define SAT_2 "DO-111"
#define VERSION "0.2b 2021-11-13"
char string[625];

void handler()
{
	/* This is a function that is called when the response function
	   times out.  This is in case the server fails to respond. */

	signal(SIGALRM,handler);
}

int connectsock(char *host, char *service, char *protocol)
{
	/* This function is used to connect to the server.  "host" is the
	   name of the computer on which PREDICT is running in server mode.
	   "service" is the name of the socket port.  "protocol" is the
	   socket protocol.  It should be set to UDP. */

	struct hostent *phe;
	struct servent *pse;
	struct protoent *ppe;
	struct sockaddr_in sin;
	
	int s, type;
	
	bzero((char *)&sin,sizeof(struct sockaddr_in));
	sin.sin_family=AF_INET;
	
	if ((pse=getservbyname(service,protocol)))
		sin.sin_port=pse->s_port;

	else if ((sin.sin_port=htons((unsigned short)atoi(service)))==0)
	{
		printf("Can't get services\n");
		return -1;
	}

	if ((phe=gethostbyname(host)))
		bcopy(phe->h_addr,(char *)&sin.sin_addr,phe->h_length);

	else if ((sin.sin_addr.s_addr=inet_addr(host))==INADDR_NONE)
	{
		printf("Can't get host: \"%s\".\n",host);
		return -1;
	}
	
	if ((ppe=getprotobyname(protocol))==0)
		return -1;

	if (strcmp(protocol,"udp")==0)
		type=SOCK_DGRAM;
	else
		type=SOCK_STREAM;
	
	s=socket(PF_INET,type,ppe->p_proto);

	if (s<0)
	{
		printf("Can't get socket.\n");
		return -1;
	}

	if (connect(s,(struct sockaddr *)&sin,sizeof(sin))<0)
	{
		printf("Can't connect to socket.\n");
		return -1;
	}

	return s;
}

void get_response(int sock, char *buf)
{
	/* This function gets a response from the
	   server in the form of a character string. */

	int n;

	n=read(sock,buf,625);

	if (n<0)
	{
		if (errno==EINTR)
			return;

		if (errno==ECONNREFUSED)
		{
			fprintf(stderr, "Connection refused - PREDICT server not running\n");
			exit (1);
		}
	}

	buf[n]='\0';
}

char *send_command(host, command)
char *host, *command;
{
	int sk;

	/* This function sends "command" to PREDICT running on
	   machine "host", and returns the result of the command
	   as a pointer to a character string. */

	/* Open a network socket */
	sk=connectsock(host,"predict","udp");

	if (sk<0)
		exit(-1);

	/* Build a command buffer */
	sprintf(string,"%s\n",command);

	/* Send the command to the server */
	write(sk,command,strlen(string));

	/* clear string[] so it can be re-used for the response */
	string[0]=0;

	/* Get the response */
   	get_response(sk,string);

	/* Close the connection */
   	close(sk);

	return string;
}

void send_command2(host, command)
char *host, *command;
{
	int sk;

	/* This function sends "command" to PREDICT running on
	   machine "host", and displays the result of the command
	   on the screen as text.  It reads the data sent back
	   from PREDICT until an EOF marker (^Z) is received. */

	/* Open a network socket */
	sk=connectsock(host,"predict","udp");

	if (sk<0)
		exit(-1);

	/* Build a command buffer */
	sprintf(string,"%s\n",command);

	/* Send the command to the server */
   	write(sk,command,strlen(string));
	/* clear string[] so it can be re-used for the response */
	string[0]=0;

	/* Read and display the response until a ^Z is received */
   	get_response(sk,string);

	while (string[0]!=26)  /* Control Z */
	{
		printf("%s",string);
		string[0]=0;
   		get_response(sk,string);
	}

	printf("\n");

	/* Close the connection */
   	close(sk);
}

int savefile(char *data)
{
	FILE *fptr;
	//test file exist
	if((fptr = fopen(FILENAME,"r"))!=NULL)
        {
		// file exists
	        fclose(fptr);
        }
    else
        {
	        //File not found, no memory leak since 'file' == NULL
	        //fclose(file) would cause an error
		fptr = fopen(FILENAME, "a");
		fprintf(fptr, "\"TIME\",");
		fprintf(fptr, "\"NAME\",\"LONG\",\"LAT\",\"AZ\",\"EL\",\"NEXT AOS/LOS\",\"FOOTPRINT\",\"RANGE\",\"ALTITUDE\",\"VELOCITY\",\"ORBIT NUMBER\",\"VISIBILITY\",\"ORBITAL PHASE\",\"ECLIPSE DEPTH\",\"SQUINT\",");
		fprintf(fptr, "\"NAME\",\"LONG\",\"LAT\",\"AZ\",\"EL\",\"NEXT AOS/LOS\",\"FOOTPRINT\",\"RANGE\",\"ALTITUDE\",\"VELOCITY\",\"ORBIT NUMBER\",\"VISIBILITY\",\"ORBITAL PHASE\",\"ECLIPSE DEPTH\",\"SQUINT\"\n");
		fclose(fptr);
        }
	fptr = fopen(FILENAME, "a");
	fprintf(fptr, "%s", data);
	fclose(fptr);
	return(0);
}

char *formatcsv(char *inputdata)
{
	int n=0, m=0;
	static char outdata[625];
	outdata[0] = 0x00;
	while (inputdata[n] != 0x0a)
	{
		//outdata[m] = '\"';
		//m++;
		while (inputdata[n] != 0x0a) {outdata[m] = inputdata[n]; n++; m++;}
		//outdata[m] = '\"';
		//m++;
		outdata[m] = ',';
		m++;
		n++;
	}
	outdata[m-1]=0x00;
	//sprintf(outdata, "%s", outdata);
	
	return outdata;	
}

int main(argc,argv)
char argc, *argv[];
{
	int i, x, y, interv = 10;
	char command[128], server[50] = "localhost";
	char data[1000], csvdata[1000], dataline_1[1000], dataline_2[1000], dataline_3[1000];
	y=argc-1;
	
	/* Scan command-line arguments */
	for (x=1; x<=y; x++)
	{
		if (strcmp(argv[x], "-i")==0) interv = atoi(argv[x+1]);
		if (strcmp(argv[x], "-s")==0) sprintf(server, "%s", argv[x+1]);
	}

	printf("Version: %s\n", VERSION);
	printf("Interval: %d min\n", interv);


	/* The following command is used to initially "ping"
	   the PREDICT server to see if connectivity exists.
	   If this test fails, the program will exit with an
	   error message. */

	printf("Predict version: %s", send_command(server, "GET_VERSION"));

	//fgets(command, 80, stdin);

	//while (strncmp(command, "QUIT", 4) && strncmp(command, "quit", 4) && strncmp(command, "EXIT", 4) && strncmp(command, "exit", 4))
	while (-1)
	{

		sleep(interv * 60);
		for(i=0; i < 3; i++)
		{

			command[strlen(command)-1]=0;

			if (i == 0) sprintf(command, "GET_TIME\n");
			if (i == 1) sprintf(command, "GET_SAT %s\n", SAT_1);
			if (i == 2) sprintf(command, "GET_SAT %s\n", SAT_2);

			//if (strncmp(command, "PREDICT",7)==0 || strncmp(command, "GET_SAT_POS",11)==0)
				//send_command2("localhost", command);
			//else if (command[0])
			if (command[0])
			{
				sprintf(data, "%s\n",send_command("localhost", command));
				//printf("\ni=%d\n%s\n", i, data);
				//printf("\nCommand Number: %d data[0]: %d\n", i, data[0]);
				if (data[0]!=0x0a)
				{
				    sprintf(csvdata, "%s", formatcsv(data));					//RAW adatok formazasa
				    if (i == 2) csvdata[strlen(csvdata)-1] = 0x0a; 				//ha az utolso beolvasas akkor vesszo helyett \n
				    else csvdata[strlen(csvdata)-1] = ',';
				    printf("%s\n", csvdata);
				    //Store data
				    if (i == 0) sprintf(dataline_1, "%s", csvdata);
				    if (i == 1) sprintf(dataline_2, "%s", csvdata);
				    if (i == 2) sprintf(dataline_3, "%s", csvdata);
				    if (dataline_2[0] != 0x0a && dataline_2[0] != 0x0a && i == 2) {savefile(dataline_1); savefile(dataline_2); savefile(dataline_3);}
				}
				else
				{
				    if (i == 1) printf("\n\nERROR! No %s sat data...\nPLEASE CHECK THE predict.tle FILE AND RELOAD PREDICT!\n\n", SAT_1);
				    if (i == 2) printf("\n\nERROR! No %s sat data...\nPLEASE CHECK THE predict.tle FILE AND RELOAD PREDICT!\n\n", SAT_2);
				    exit(-1);
				}
			}
		
		}
	
	}
	exit(0);
}


## VERSION: 0.2b 2021-11-13

# Leírás

Ez a program lekérdezi a PREDICT szervert és összegyűjti az MO-110 (SMOG-1) és DO-111 (DIY 1) műholdak aktuális adatait és menti a helyi könyvtárba CSV formátumban.
Az -i kapcsolóval megadható a lekérdezés intervalluma. Az alapértelmezett intervallum 10 perc.
Az adatgyűjtő rész szerzője: HA71970

## Előkészületek

**Fontos**

A program az adatokat a predict (KD2BD) nevű szatellit predikciós és követőprogramból hívja le, ezért annak telepítve kell lennie és szerver módban futnia kell!

A predict beszerzési lehetőségei:<br>
https://github.com/kd2bd/predict<br>
https://www.qsl.net/kd2bd/predict.html

esetleg:

    sudo apt install predict

A predict indítása

    cd predict-2.2.7
    ./predict -s

Ahhoz hogy a minket érdeklő szatellitek adatait le lehessen kérdezni, a predict-et ennek megfelelően kell konfigurálni.
Ehhez a default könyvtárban található fájlokat be kell másolni a HOME könyvtár (amelyik user alatt futtatjuk a predict-et) _.predict_
alkönyvtárba.

    cd datacollector/default
    cp * ~/.predict
    
Természetesen a predict.qth fájlt értelemszerűen testre kell szabni.
Ezután időről-időre frissíteni kell a _predict.tle_ fálj tartalmát, a predict szoftverhez mellékelt manuál alapján.

<hr>

# Program telepítése, használata

## beszerzés

    cd somewhere
    git clone https://gitlab.com/szoky/datacollector

## frissítés

    cd datacollector
    git pull

## Fordítás

    cd datacollector
    ./build

## Indítás:

    cd datacollector
    ./satdcollect -i m

Ahol m a lekérdezési időintervallum percben. Ha ez nincs megadva, akkor az alapértelmezett időtartam 10 perc.

## Adatfeldolgozás:

A gyűjtött adatok a helyi könyvtárban létrejött _satdatalog.csv_ fájlban találhatók. Ezt könnyen táblázatkezelőbe lehet importálni.

Ezt a fájlt a program az első indításkor a mentési periódusban hozza létre és beleírja a táblázat oszlopainak címkéit.
Ezután minden mentéskor újabb sorokat fűz az adatfájlhoz (append). Ha ez a fájl már létezik, akkor a program nem ír bele újabb fejléc információkat, mentéskor csak hozzáfüz a meglévő tartalomhoz. 

## Gyűjtött adatok

- Name           (Name of satellite)
- Long           (degrees West)
- Lat            (degrees North)
- Az             (degrees)
- El             (degrees)
- Next AOS/LOS   (seconds since 01-Jan-1970)
- Footprint      (kilometers)
- Range          (kilometers)
- Altitude       (kilometers)
- Velocity       (kilometers/hour)
- Orbit Number   (revolutions)
- Visibility     (see below)
- Orbital Phase  (degrees)
- Eclipse Depth  (degrees)
- Squint         (degrees, or 360.0 if squint is not applicable)

## Kapcsolók

    -i [time] Lekérdezések intervalluma percben (default: 10 percenként)
    -s [address] Predict server host IP címe (default: localhost)  

## Kommunikáció

Default predict server : localhost<br>
Protokoll: UDP<br>
Port: 1210<br>


Sikeres munkát!
